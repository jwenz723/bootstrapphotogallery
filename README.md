# What is this repository? #

This is a simple web page built demonstrating the use of Bootstrap to build a responsive image gallery.  It utilizes jQuery to handle click events and replacing the viewable content on the page.  It uses a .json file containing image contents/descriptions to dynamically populate all of the images and their respective data on the page.